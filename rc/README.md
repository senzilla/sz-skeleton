# Services

## Dependency graph

The `00` service is special. It should always be at the bottom of the
dependency graph.

## Standard bundles

The `ok-*` services provide a set of standard bundles that other
services should depend on for their generic needs.

### ok-disks

Logging services should always depend on `ok-disks` to ensure there is
a writable filesystem available.

It is up to the administrator to select what service that provide
writable filesystem as part of this bundle.

### ok-sshd

This provides a generic sshd bundle that any sshd implementation can
declare to be `part-of`. This enables other services to generically
depend on `ok-sshd` for any ssh daemon.

It is up to the administrator to select what service that provides a
ssh daemon as part of this bundle.

## Kconfig notes

Service configuration should always `select` services that are listed
in `dependencies` and `part-of`.

Logging services should have `depends on` for service they consume.
