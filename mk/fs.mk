# SPDX-License-Identifier: ISC

POLICY=		../../.policy
CONFIG=		../../.config
INSTALL?=	sz-install

-include $(POLICY)
-include $(CONFIG)

BUILD=		$(if $(CONFIG_FS_SINGLE),../../build/rootfs,../../build/$(FS))
DESTDIR?=	$(BUILD)/target

FSU=		$(shell echo "$(FS)" | tr a-z A-Z)

include ../../mk/skel.mk

TARGET=		$(SOURCE:skel/%=$(BUILD)/%)
TARGET+=	$(if $(CONFIG_FS_$(FSU)_TYPE),		$(BUILD)/etc/fs/$(FS)/TYPE)
TARGET+=	$(if $(CONFIG_FS_$(FSU)_OPTIONS),	$(BUILD)/etc/fs/$(FS)/OPTIONS)
TARGET+=	$(if $(CONFIG_FS_$(FSU)_DEV),		$(BUILD)/etc/fs/$(FS)/DEV)

.PHONY: build
build: $(TARGET) $(CONFIG)

$(BUILD)/%: skel/%
	$(INSTALL) -x $(POLICY) -Dm 0644 $< $@

$(BUILD)/etc/fs/$(FS)/%: $(CONFIG)
	mkdir -p $(@D)
	echo $(CONFIG_FS_$(FSU)_$(@F)) > $@
