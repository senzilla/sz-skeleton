# SPDX-License-Identifier: ISC

OPTIONS=	$(shell cat OPTIONS)
OPTIONS_ENABLED=$(foreach o,$(OPTIONS),$(if $(filter y,$(CONFIG_$(notdir $(o)))),$(dir $(o))))

#
# Update OPTIONS by running for example:
# make print-OPTIONS NAMESPACE=RC_ > OPTIONS
#
.PHONY: print-OPTIONS
print-OPTIONS:
	@find ./* -maxdepth 0 -type d -exec basename {} \; | sort | \
	awk '{o=$$1; gsub(/-/,"_",o); print $$1 "/$(NAMESPACE)" toupper(o)}'
