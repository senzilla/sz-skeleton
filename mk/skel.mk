SOURCE=		$(shell cat skel/FILES)

#
# Update FILES by running:
# make print-FILES > skel/FILES
#
.PHONY: print-FILES
print-FILES:
	@find skel/* -not -type d -not -name FILES | sort
