s6 skeleton
===========

Goals
-----

- Re-usable s6[1] skeleton for image builders (e.g. Buildroot[2])
- Layout should be as simple and secure as possible by default
- All output should be architecture-independent files and scripts

Excplicit non-goals
-------------------

- Compile programs (your build system or package manager should do that)

Dependencies
------------

Build-time dependencies on the host:

- make
- execline
- s6-portable-utils
- s6-linux-init-maker
- s6-rc-compile
- sed
- tr
- find

The `.config` file is easy to create and edit with a text editor.
Alternatively, the the optional `ncurses` configuration UI can be used
on the host with the following dependencies:

- python3
- kconfiglib[3]

Run-time dependencies on the target will vary with the configuration.
But the main dependencies are:

- s6-linux-init
- s6-linux-utils
- s6-rc
- /bin/sh

[1]: http://skarnet.org/software/s6
[2]: https://buildroot.org/
[3]: https://pypi.org/project/kconfiglib/1.0.1/
