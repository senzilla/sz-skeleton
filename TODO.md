# TODO

In-depth audit of service dependencies.

More abstract service configuration

1. Build out /etc/config/{fs,net,start} etc.

More abstract networking configuration and services:

1. Build out ./net and envdir /etc/config/net
2. Implement ok-dhcpc bundle
3. Implement ok-dhcpd bundle
