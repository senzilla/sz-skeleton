# SPDX-License-Identifier: ISC

-include .policy
-include .config

BUILD?=		build
DESTDIR?=	$(BUILD)/target

.PHONY: build
build: $(BUILD)/.sz_build
	$(foreach dir,fs init rc,$(MAKE) -C $(dir)/ $@;)

.PHONY: finalize
finalize: build
	$(foreach dir,rc init fs,$(MAKE) -C $(dir)/ $@;)
	find $(@D) -name ".sz_*" -exec rm {} +

$(BUILD)/.sz_build:
	mkdir -p $(BUILD)/rootfs
	$(if $(CONFIG_FS_SINGLE),ln -s rootfs $(BUILD)/rwfs)
	touch $@

.PHONY: install
install: $(DESTDIR)/sbin/init

$(DESTDIR)/sbin/init:
	mkdir -p $(@D)
	cp -a $(BUILD)/rootfs/* $(@D)/

.PHONY: clean
clean:
	rm -rf $(BUILD)

.PHONY: menuconfig oldconfig
menuconfig oldconfig:
	$@

.PHONY: users-table
users-table:
	sz-br-table -u $(BUILD)/users $(BUILD)/users-table.txt

.PHONY: permissions-table
permissions-table:
	sz-br-table $(BUILD)/permissions $(BUILD)/permissions-table.txt

REL_MAKEFILE=	0
-include sz.release.mk
